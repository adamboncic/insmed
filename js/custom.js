      /*
        // Note from Cat //
        I'm adding this to make a function for the pop ups.
        what happens is I will use the id as a key and then make a json item with the
        paramaters (image url, title text if it exists and height and width so it can be changed easily)

      */

      var popUpData = {
        //Branch 1 - Tab 1
        "zoom-1-c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/branch1-spread1-1.png"
        },
        "zoom-2-c": {
          "height": 3000,
          "width": 814,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/branch1-spread1-2.png"
        },
        "pop-bn-1-c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" +
              "<h3>Context (for your background information):</h3>  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." + 
              "</br></br></br><h3>Key Points:</h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. " +
            "</div>"
        },
        "pop-bn-2-c": {
          "height": 650,
          "width": 1410,
          "left": 0,
          "top": 8,  
          "bottom": 0,
          "right": 0,  
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/2-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 2%;'>" +
          "<h3>Context (for your background information):</h3> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " + 
          "</br></br><h3>Key Points:</h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</br></br>Sit amet volutpat consequat mauris nunc congue nisi. Volutpat odio facilisis mauris sit. Pretium quam vulputate dignissim suspendisse. Ut tristique et egestas quis ipsum suspendisse ultrices. Id neque aliquam vestibulum morbi. Velit sed ullamcorper morbi tincidunt ornare massa eget. Scelerisque fermentum dui faucibus in ornare quam viverra. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Id ornare arcu odio ut. Aliquam id diam maecenas ultricies mi eget. Convallis a cras semper auctor neque vitae tempus quam pellentesque. Vulputate sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum." +
        "</div>" + "<img id='qr-code' src='img/Workshop1_1 - SL101.png'>"
        },
        "pop-bn-3-c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/3-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 5%;'>" + 
          "<h3>Context (for your background information):</h3> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " 
          + "</div>"
        },
        // Branch 1 - Tab 2
        "zoom-1-2c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread2-1.png"
        },
        "zoom-2-2c": {
          "height": 3000,
          "width": 1660,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread2-2.png"
        },
        "zoom-3-2c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread2-3.png"
        },
        "pop-bn-2-1c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer enim neque volutpat ac tincidunt vitae semper quis lectus." + "</div>"
        },
        "pop-bn-2-2c": {
          "height": 650,
          "width": 1410,
          "left": 0,
          "top": 8,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/2-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 3%;'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </br></br>Sit amet volutpat consequat mauris nunc congue nisi. Volutpat odio facilisis mauris sit. Pretium quam vulputate dignissim suspendisse. Ut tristique et egestas quis ipsum suspendisse ultrices. Id neque aliquam vestibulum morbi. Velit sed ullamcorper morbi tincidunt ornare massa eget. Scelerisque fermentum dui faucibus in ornare quam viverra. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Id ornare arcu odio ut. Aliquam id diam maecenas ultricies mi eget. Convallis a cras semper auctor neque vitae tempus quam pellentesque. Vulputate sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum. Eget nunc lobortis mattis aliquam faucibus purus in massa. Cum sociis natoque penatibus et. Arcu odio ut sem nulla pharetra diam sit amet. Fermentum et sollicitudin ac orci phasellus egestas. Pretium lectus quam id leo in vitae." + "</div>" +
          "<img id='qr-code' src='img/Workshop1_2 - SL102.png'>"
        },
        // Branch 1 - Tab 3
        "zoom-1-3c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread3-1.png"
        },
        "zoom-2-3c": {
          "height": 3000,
          "width": 1660,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread3-2.png"
        },
        "zoom-3-3c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread3-3.png"
        },
        "pop-bn-3-3c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,   
          "background-img": "img/3-modal-small.png",
          "html": "<div id='pop-bn-small-modal' style='top: 13%;'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer enim neque volutpat ac tincidunt vitae semper quis lectus. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere. Bibendum arcu vitae elementum curabitur vitae nunc sed." +
          "</br></br><h3>Key Points:</h3>" + "Orci dapibus ultrices in iaculis nunc. Consequat nisl vel pretium lectus quam id leo. Diam maecenas sed enim ut sem viverra. Nunc id cursus metus aliquam eleifend mi. Erat velit scelerisque in dictum non consectetur a erat nam. Pulvinar elementum integer enim neque volutpat ac tincidunt. Purus sit amet volutpat consequat mauris. Vitae congue eu consequat ac felis. Ultrices in iaculis nunc sed augue.>" + "</div>"
        },
        "pop-bn-3-4c": {
          "height": 650,
          "width": 1410,
          "left": 0,
          "top": 8,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/4-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 6%;'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer enim neque volutpat ac tincidunt vitae semper quis lectus." + 
          "Rhoncus urna neque viverra justo nec ultrices. Purus non enim praesent elementum facilisis leo vel fringilla. Tellus in metus vulputate eu scelerisque. Tempus iaculis urna id volutpat lacus laoreet. Ultrices dui sapien eget mi proin sed. Euismod quis viverra nibh cras pulvinar mattis nunc sed. Porta non pulvinar neque laoreet suspendisse. Neque sodales ut etiam sit amet nisl purus in mollis. Cras pulvinar mattis nunc sed blandit libero volutpat. Sodales neque sodales ut etiam sit. Metus vulputate eu scelerisque felis. Tortor condimentum lacinia quis vel eros donec ac odio tempor. Egestas sed sed risus pretium quam vulputate dignissim suspendisse." + 
          "<h3>Key Points:</h3>" + "Tempus iaculis urna id volutpat lacus laoreet. A pellentesque sit amet porttitor eget dolor morbi non arcu." +  "</div>"
        },
        // Branch 1 - Tab 4
        "zoom-1-4c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread4-1.png"
        },
        "zoom-2-4c": {
          "height": 3000,
          "width": 1660,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread4-2.png"
        },
        "zoom-3-4c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread4-3.png"
        },
        "pop-bn-4-5c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/5-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer enim neque volutpat ac tincidunt vitae semper quis lectus." + "</div>"
        },
        // Branch 1 - Tab 5
        "zoom-1-5c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread5-1.png"
        },
        "zoom-2-5c": {
          "height": 3000,
          "width": 1735,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread5-2.png"
        },
        "pop-bn-5-1c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png", 
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." + 
          "</br></br><h3>Key Points:</h3>" + "Sit amet volutpat consequat mauris nunc congue nisi. Volutpat odio facilisis mauris sit. Pretium quam vulputate dignissim suspendisse. Ut tristique et egestas quis ipsum suspendisse ultrices. Id neque aliquam vestibulum morbi. Velit sed ullamcorper morbi tincidunt ornare massa eget. Scelerisque fermentum dui faucibus in ornare quam viverra. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Id ornare arcu odio ut. " + "</div>" + "<img id='qr-code' src='img/Workshop1_3 - SL103.png'>"
        },
        // Branch 1 - Tab 6
        "zoom-1-6c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread6-1.png"
        },
        "zoom-2-6c": {
          "height": 3000,
          "width": 1735,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread6-2 (1).png"
        },
        "zoom-3-6c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread6-3.png"
        },
        "pop-bn-6-2c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/competition graphic_large.png",
          "html": "<div id='pop-bn-small-modal'>" + "</div>" + "<img id='qr-code' src='img/Workshop1_4 - SL104.png'>"
        },
        // Branch 1 - Tab 7
        "zoom-1-7c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread7-1.png"
        },
        "zoom-2-7c": {
          "height": 3000,
          "width": 1735,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread7-2.png"
        },
        "zoom-3-7c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread7-3.png"
        },
        "pop-bn-7-1c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,    
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." + 
          "</br></br><h3>Key Points:</h3>" + "Integer enim neque volutpat ac tincidunt vitae semper quis lectus. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere. Bibendum arcu vitae elementum curabitur vitae nunc sed. Orci dapibus ultrices in iaculis nunc. Consequat nisl vel pretium lectus quam id leo. Diam maecenas sed enim ut sem viverra. Nunc id cursus metus aliquam eleifend mi. Erat velit scelerisque in dictum non consectetur a erat nam. Pulvinar elementum integer enim neque volutpat ac tincidunt. Purus sit amet volutpat consequat mauris. Vitae congue eu consequat ac felis. Ultrices in iaculis nunc sed augue." + "</div>"
        },
        "pop-bn-7-2c": {
          "height": 650,
          "width": 1498,
          "left": -8,
          "top": 5,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/2-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 1%;width: calc(100% - 25rem);'>" + "<h3>Context (for your background information):</h3>" +
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer enim neque volutpat ac tincidunt vitae semper quis lectus. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere. Bibendum arcu vitae elementum curabitur vitae nunc sed. Orci dapibus ultrices in iaculis nunc. Consequat nisl vel pretium lectus quam id leo. Diam maecenas sed enim ut sem viverra. Nunc id cursus metus aliquam eleifend mi. Erat velit scelerisque in dictum non consectetur a erat nam. Pulvinar elementum integer enim neque volutpat ac tincidunt. Purus sit amet volutpat consequat mauris. Vitae congue eu consequat ac felis. Ultrices in iaculis nunc sed augue." +
          "</br></br>Sit amet volutpat consequat mauris nunc congue nisi. Volutpat odio facilisis mauris sit. Pretium quam vulputate dignissim suspendisse. Ut tristique et egestas quis ipsum suspendisse ultrices. Id neque aliquam vestibulum morbi. Velit sed ullamcorper morbi tincidunt ornare massa eget. " + 
          "<h3>Key Points:</h3>" + "Scelerisque fermentum dui faucibus in ornare quam viverra. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Id ornare arcu odio ut. " + "</div>" +
          "<img style='right:-13%;' id='qr-code' src='img/Workshop1_5 - SL105.png'>" +
          "<img style='right:-13%;' id='qr-code' src='img/PLACEHOLDER QR.png'>"
        },
        "pop-bn-7-3c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/3-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 5%;'>" + "<h3>Context (for your background information):</h3>" +
          "Rhoncus urna neque viverra justo nec ultrices. Purus non enim praesent elementum facilisis leo vel fringilla. Tellus in metus vulputate eu scelerisque. " + 
          "</br></br>Tempus iaculis urna id volutpat lacus laoreet. Ultrices dui sapien eget mi proin sed. Euismod quis viverra nibh cras pulvinar mattis nunc sed. " +
          "</br></br><h3>Key Points:</h3>" + "Porta non pulvinar neque laoreet suspendisse. Neque sodales ut etiam sit amet nisl purus in mollis. Cras pulvinar mattis nunc sed blandit libero volutpat. Sodales neque sodales ut etiam sit. Metus vulputate eu scelerisque felis. Tortor condimentum lacinia quis vel eros donec ac odio tempor." + "</div>"
        },
        "pop-bn-7-4c": {
          "height": 650,
          "width": 1450,
          "left": 0,
          "top": 5,  
          "bottom": 0,
          "right": 0,   
          "background-img": "img/4-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 4%; right:3%; width:calc(100%-25rem);'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer enim neque volutpat ac tincidunt vitae semper quis lectus. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere. Bibendum arcu vitae elementum curabitur vitae nunc sed. Orci dapibus ultrices in iaculis nunc. Consequat nisl vel pretium lectus quam id leo. Diam maecenas sed enim ut sem viverra." + 
          "</br></br>Nunc id cursus metus aliquam eleifend mi. Erat velit scelerisque in dictum non consectetur a erat nam. Pulvinar elementum integer enim neque volutpat ac tincidunt. Purus sit amet volutpat consequat mauris. Vitae congue eu consequat ac felis. Ultrices in iaculis nunc sed augue." + 
          "</ul>" + "<h3>Key Points:</h3>" + 
          "Sit amet volutpat consequat mauris nunc congue nisi. Volutpat odio facilisis mauris sit. Pretium quam vulputate dignissim suspendisse. Ut tristique et egestas quis ipsum suspendisse ultrices. Id neque aliquam vestibulum morbi. "
          + "</div>"

        },
        // Branch 1 - Tab 8
        "zoom-1-8c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread8-1.png"
        },
        "zoom-2-8c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread8-2.png"
        },
        "zoom-3-8c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread8-3.png"
        },
        "pop-bn-8-1c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer enim neque volutpat ac tincidunt vitae semper quis lectus. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere. Bibendum arcu vitae elementum curabitur vitae nunc sed. Orci dapibus ultrices in iaculis nunc. Consequat nisl vel pretium lectus quam id leo. Diam maecenas sed enim ut sem viverra. " + "</div>" + "<img id='qr-code' src='img/Workshop1_6 - SL106.png'>"
        },
        "star-bn-pa-1-v": {
          "height": 650,
          "width": 950,
          "left": 0,
          "top": 16,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='right: 10%; top: 12%;'>" + "The Therapeutic Specialist can inform HCP offices:" + 
          "<ul><li id='pop-bn-small-plain-indent'>That PA forms for ARIKAYCE are available on CoverMyMeds</li>" + 
          "<li id='pop-bn-small-plain-indent'>To contact CoverMyMeds directly for any questions</li>" + "</ul>" + "</div>"
        },
        "star-bn-pa-3-v": {
          "height": 650,
          "width": 1180,
          "left": 0,
          "top": 13,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='top: 1%; left:10%;'>" + "<ul><li>Arikares Coordinator has visibility into the PA through coverage determination</li>" + 
          "<li>Arikares Coordinator monitors PA progress through manual follow-up with HCP</li>" + 
          "<li>Asembia & Specialty Pharmacy follow-up with payer to confirm receipt/determination</li>" + "</ul>" 
          + "<div id='PA-box'>If a patient is not enrolled in the Arikares Support Program, the PA process is coordinated with the HCP office by" + 
          " the specialty pharmacy. The Arikares Coordinator will not have visibility into PA status through CoverMyMeds.</div>"
          + "</div>"
          + "<img id='qr-code' src='img/Workshop3_9 - SL309.png' style='right: -22%; z-index: 2;'>" 
          + "<img id='qr' src='img/PLACEHOLDER QR.png' style='right: -22%;'>"
        },
        "star-bn-pa-4-v": {
          "height": 650,
          "width": 1450,
          "left": 0,
          "top": 6,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='right: 8%; top: 10%;'>" + 
          "<ul><li style='top: -9%; position: absolute;'>Therapeutic Specialist has visibility into PA submission status of each patient (deidentified within his/her territory through 'Operational Alerts'</li>" +  
          "</ul>" + "<div style='word-wrap: break-word; width: 280px; font-size:27px;position: absolute; z-index: 2; right: -1.3%; top:20%;'>Overall <b>Territory Level</b> summary of Patient Enrollment Status</div>" + 
          "<div style='word-wrap: break-word; width: 314px; font-size:27px; position: absolute; z-index: 2; right: -3.5%; top:62%;'><b>Patient Level</b> Enrollment status with details around BI/BV, PA submission and shipment status</div>" + "<img style='box-shadow: none; height: 82%; width: 106%; margin-top: 63px; position: absolute; left: -18px;' src='img/elrkuhf.png'>" 
          + "</div>"
        },
        "star-bn-pa-5-v": {
          "height": 650,
          "width": 1305,
          "left": 0,
          "top": 11,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='top: -2%;padding:10px;'>" + "<ul><li>The Arikares Coordinator will primarily follow up about prior authorization with an HCP office</li>" + 
          "<li>The Arikares Coordinator may escalate an HCP bottleneck issue to you for follow-up when necessary. You are expected to help out and assist in a timely and compliant manner and report back to the Arikares Coordinator</li>" + 
          "<li>If you identify access challenges that may require follow up with a payer, you should communicate these situations with your Regional Director and KAD</li>" +
          "<li>Remember, you are a member of the field sales team for Insmed, and will not be representing the Patient Services organization. You will, however, follow approved guidelines when discussing Support Program details with customers</li>"
          + "</ul>" + "</div>"
          + "<img id='qr-code' src='img/Workshop3_9 - SL309.png' style='right: -18%; z-index: 2;'>" 
          + "<img id='qr' src='img/PLACEHOLDER QR.png' style='right: -18%;'>"
        },
        "star-bn-pa-6-v": {
          "height": 650,
          "width": 1305,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='top: -7%;padding:19px;'>" + "<ul><li>If PA was denied, the HCP/patient can decide to appeal payer decision. The appeals process is not managed within CoverMyMeds</li>" + 
          "<li>Information available for HCP offices in the HCP Portfolio (see below)</li>" +
          "<img style='width: 24rem; box-shadow: none;' src='img/01 HCP Portfolio Closed.png' alt='hcp closed'>" +
          "<img style='width: 35rem; box-shadow: none; margin-top: -56px;' src='img/04 HCP Portfolio Full Open.png' alt='hcp open'>" +
          "<li>This information is also available on the Arikares section of HCP website</li>"
          + "</ul>" + "</div>"
          + "<img id='qr-code' src='img/Workshop3_10 - SL310.png' style='right: -18%; z-index: 2;'>" 
          + "<img id='qr' src='img/PLACEHOLDER QR.png' style='right: -18%;'>"
        },
        "star-bn-pa-7-v": {
          "height": 650,
          "width": 1305,
          "left": 0,
          "top": 12,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='top: 3%;padding:10px;'>" + "The Arikares Coordinator will contact the patient to discuss their approval and financial obligations"
           + "<div id='row1'>" + "<div id='column1'><h3>For Commercial or Private Insurance:</br>ARIKAYCE Co-pay Savings Program*</h3>" + 
           "<ul><li>Eligible patients using private or commercial insurance can save on out-of-pocket costs for ARIKAYCE</li>" + 
           "<li>Most patients who are eligible, pay $0 co-pay every month, up to a $32,000 maximum program benefit limit of $8,000 per month. Patient will be responsible for any co-pay once limit is reached. Depending on the private or commercial health insurance plan, savings may apply toward co-pay, co-insurance, or deductible</li>" +
           "<li>Not valid for prescriptions covered by or submitted for reimbursement under Medicaid, Medicare, VA, DOD, TRICARE or similar federal or state programs, including any state pharmaceutical assistance program</li>" +
           "<li>Eligibility can be determined by calling Arikares at 1-833-ARIKARE (1-833-274-5273).</li>" +
           "<li>*See full terms and conditions</li>" + "</ul>" + "</div>" + 
           "<div id='column2'><h3>For Government Coverage:</h3><ul><li>If you receive questions or concerns about Medicare Part D coverage, use approved resources to discuss financial assistance programs that such patients may be eligible for</li></ul></div>" 
           + "</div>" + "</div>"
        },
        "star-bn-1-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/Branch 2-HCP-Popover1 (2).png",
          "html": "<div class='fragment' data-fragment-index='11' data-autoslide='2000'>" +
          "<button type='button' data-animate='pulseAlt' id='star-bn-1-v1' class='open-popup-btn'></button>" + 
          "</div>" + "<img id='qr-code' src='img/Workshop3_2 - SL302.png'>"
        },
        "star-bn-2-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/Branch 2-HCP-Popover2.png",
          "html": "<img id='qr-code' src='img/Workshop3_4 - SL304.png'>"
        },
        "star-bn-3-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "display": "block",
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/Branch 2-HCP-Popover3 (1).png",
          "html": "<div class='fragment' data-fragment-index='11' data-autoslide='2000'>" +
          "<button type='button' data-animate='pulseAlt' id='star-bn-3-v1' class='open-popup-btn'></button>" + 
          "</div>" +
          "<div class='fragment' data-fragment-index='11' data-autoslide='2000'>" +
          "<button type='button' data-animate='pulseAlt' id='star-bn-3-v2' class='open-popup-btn'></button>" + 
          "</div>" +
          "<div class='fragment' data-fragment-index='11' data-autoslide='2000'>" +
          "<button type='button' data-animate='pulseAlt' id='star-bn-3-v3' class='open-popup-btn'></button>" + 
          "</div>" + "<img id='qr-code' src='img/Workshop3_5 - SL305.png'>" 
        },
        "star-bn-4-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/Branch 2- HCP-Popover2.png"
        },
        "star-bn-1-v1": {
          "height": 650,
          "width": 1450,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-large-plain.png",
          "html": "<div id='pop-bn-large-plain' style='top: 0%;'>" + "<p style='font-size: 35px;'>When talking with HCPs, use the following talking points as an overview of the program:</p>" + 
          "<div id='p-box'>" + "<h3><em>The Arikares Support Program supports ARIKAYCE patients during their treatment journey. " + 
          "Once a patient is enrolled in the Arikares Support Program:</em></h3>" +
          "<ul><b><em><li>The Arikares Coordinator can:</li>" + 
          "<li id='pop-bn-large-plain-indent'>Provide assistance with payer navigation, information, prior authorization, and prescription access</li>" +
          "<li id='pop-bn-large-plain-indent'>Help patients appropriately manage their therapy as prescribed for the duration of treatment</li>" +
          "<li id='pop-bn-large-plain-indent'>Will work with an appropriate specialty pharmacy to process and ship ARIKAYCE to a patient's home</li>" + 
          "<li>Arikares Trainers (nurses and respiratory therapists) may provide an in-home training at treatment initiation</li>" +
          "<li>Arikares can provide patients with appropriate encouragement, as well as ongoing support during their course of care</li>" + "</em></b></ul>" + "</div>"
          + "</div>" + "<img style='right:-14%; z-index:2;' id='qr-code' src='img/Workshop3_3 - SL303.png'>" +
          "<img style='right:-14%;' id='qr' src='img/PLACEHOLDER QR.png'>"
        },
        "star-bn-3-v1": {
          "height": 650,
          "width": 950,
          "left": 0,
          "top": 16,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-plain' style='top: 29%; right: 2%;'>" + "The information on this form must be completed by the patient, " + 
          "their healthcare provider, and the office staff. As a Therapeutic Specialist, you should not complete any information " + 
          "on this form." + "</div>" + "<div id='backdrop'>"+ "<img  src='img/Branch 2-HCP-Popover3 (1).png'>"+ "</div>"
        },
        "star-bn-3-v2": {
          "height": 650,
          "width": 950,
          "left": 0,
          "top": 16,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/2-modal-small.png",
          "html": "<div id='pop-bn-small-plain' style='top: 30%; right: 2%;'>" + "This is a front and back form with patient information on page 1 and prescriber " + 
          "information on page 2. All required fields are marked with an asterisks." + "</div>" + "<div id='backdrop'>"+ "<img  src='img/Branch 2-HCP-Popover3 (1).png'>"+ "</div>"
        },
        "star-bn-3-v3": {
          "height": 650,
          "width": 950,
          "left": 0,
          "top": 16,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/3-modal-small.png",
          "html": "<div id='pop-bn-small-plain' style='top: 30%; right: 2%;'>" + "This form requires 2 patient signatures - one for sharing patient health " + 
          "information with Insmed and our 3rd party partners, and another for enrolling the services offered by the program." + "</div>" + "<div id='backdrop'>"+ "<img  src='img/Branch 2-HCP-Popover3 (1).png'>"+ "</div>"
        },
        "star-bn-1-patient-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png", 
          "background-img": "img/1-modal-large-plain.png",
          "html": "<div id='pop-bn-large-plain' style='top: 2%;padding:19px;font-size:32px;'>" + "The Welcome Pack arrives to the patient's home shortly after program enrollment with personalized information about their Arikares Coordinator. It provides useful information about resources related to ARIKAYCE, NTM, and the Arikares program." +
          "<img style='margin: 0 auto; box-shadow: none; display: flex; justify-content: center; align-items: center; width:82%; margin-top: 11px;' src='img/Branch 2-Patient Experience-Popover1 (2).png'>" + "</div>" + "<img id='qr-code' src='img/Workshop3_7 - SL307.png'>"
        },
      };

     